<?php
/**
 * Sets up theme defaults and registers support for various WordPress features.
 */

if ( ! function_exists( 'wpstarter_setup' ) ) :

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function wpstarter_setup() {

		// Make theme available for translation.
		load_theme_textdomain( 'TRANSLATE', WPSTARTER_THEME_DIR . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Let WordPress manage the document title.
		 *
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/**
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// Switch default core markup for search form, comment form, and comments to output valid HTML5.
		add_theme_support( 'html5', [ 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ] );

		/**
		 * Register all menus.
		 */
		register_nav_menus(
			[
				'primary' => esc_html__( 'Primary Navigation', 'TRANSLATE' ),
			]
		);

	}

endif;

add_action( 'after_setup_theme', 'wpstarter_setup' );

if ( ! function_exists( 'wpstarter_enqueue_assets' ) ) :

	/**
	 * Enqueue assets.
	 */
	function wpstarter_enqueue_assets() {

		// CSS.
		wp_enqueue_style( 'wpstarter-style', get_stylesheet_uri(), [], '1.0.0', 'all' );

		if ( is_user_logged_in() ) :
			wp_enqueue_style( 'wpstarter-styles-logged-in-min', get_stylesheet_directory_uri() . '/styles-logged-in.min.css', [], '1.0.0', 'all' );
		else :
			wp_enqueue_style( 'wpstarter-styles-min', get_stylesheet_directory_uri() . '/styles.min.css', [], '1.0.0', 'all' );
		endif;

		// JS.
		if ( is_user_logged_in() ) :
			wp_enqueue_script( 'wpstarter-scripts-logged-in-min', WPSTARTER_THEME_URI . 'scripts-logged-in.min.js', [ 'jquery' ], '1.0.0', true );
		else :
			wp_enqueue_script( 'wpstarter-scripts-min', WPSTARTER_THEME_URI . 'scripts.min.js', [ 'jquery' ], '1.0.0', true );
		endif;

		// Deregister block-library.css.
		wp_dequeue_style( 'wp-block-library' );

		// Remove the WordPress default jquery and using a local file.
		wp_deregister_script( 'jquery' );
		wp_enqueue_script( 'jquery', WPSTARTER_THEME_URI . '/lib/jquery.js','', '3.4.1', true );

		// Deregister dashicons.css and admin-bar.css and combine it with main styles.min.css.
		if ( ! is_admin() ) :
			wp_deregister_style( 'dashicons' );
			wp_dequeue_style( 'admin-bar' );
			wp_deregister_style( 'admin-bar' );
			wp_deregister_script( 'admin-bar' );
		endif;

		// Original snippet from Contact Form 7 plugin (It's all because of combine plugin script with the theme's script to make one request less).
		if ( class_exists( 'WPCF7' ) ) :

			$wpcf7 = array(
				'apiSettings' => array(
					'root' => esc_url_raw( rest_url( 'contact-form-7/v1' ) ),
					'namespace' => 'contact-form-7/v1',
				),
			);

			if ( defined( 'WP_CACHE' ) and WP_CACHE ) {
				$wpcf7['cached'] = 1;
			}

			wp_localize_script( 'wpstarter-scripts-min', 'wpcf7', $wpcf7 );

		endif;

		// Loads extra JS.
		if ( is_singular( 'post' ) && comments_open() && get_option( 'thread_comments' ) ) :
			wp_enqueue_script( 'comment-reply' );
		endif;

	}

endif;

add_action( 'wp_enqueue_scripts', 'wpstarter_enqueue_assets' );

if ( ! function_exists( 'wpstarter_add_async_attribute' ) ) :

	function wpstarter_add_async_attribute( $tag, $handle ) {

		if ( 'wpstarter-style' === $handle ) :
			return str_replace( " rel='stylesheet'", ' rel="preload" as="style" onload="this.rel=' . "'stylesheet'" . '" ', $tag );
		else :
			return $tag;
		endif;

	}

endif;

add_filter( 'style_loader_tag', 'wpstarter_add_async_attribute', 10, 2 );


if ( ! function_exists( 'wpstarter_enqueue_admin_assets' ) ) :

	/**
	 * Enqueue admin assets.
	 */
	function wpstarter_enqueue_admin_assets( $hook ) {

		// Media.
		! did_action( 'wp_enqueue_media' ) ? wp_enqueue_media() : '';

	}

endif;

add_action( 'admin_enqueue_scripts', 'wpstarter_enqueue_admin_assets' );

if ( ! function_exists( 'wpstarter_remove_jquery_migrate' ) ) :

	/**
	 * Remove JQuery migrate.
	 */
	function wpstarter_remove_jquery_migrate( $scripts ) {

		if ( ! is_admin() && isset( $scripts->registered['jquery'] ) ) :

				$script = $scripts->registered['jquery'];

				// Check whether the script has any dependencies.
				if ( $script->deps ) :
					$script->deps = array_diff( $script->deps, [
						'jquery-migrate'
					] );
				endif;

		endif;

	}

endif;

add_action( 'wp_default_scripts', 'wpstarter_remove_jquery_migrate' );

/**
* Improve security.
*
* @see http://wpmagus.pl/pliki/prezentacje/wyczaruj-sobie-spokoj/#/24
*/
remove_action( 'wp_head', 'wp_generator' );

if ( ! function_exists( 'wpstarter_secure_generator' ) ) :

	function wpstarter_secure_generator( $generator, $type ) {

		return '';

	}

endif;

add_filter( 'the_generator', 'wpstarter_my_secure_generator', 10, 2 );

if ( ! function_exists( 'wpstarter_remove_src_version' ) ) :

	/**
	* Remove src version.
	*
	* @see http://wpmagus.pl/pliki/prezentacje/wyczaruj-sobie-spokoj/#/25
	*/

	function wpstarter_remove_src_version( $src ) {

		global $wp_version;

		$version_str = '?ver='. $wp_version;
		$offset = strlen( $src ) - strlen( $version_str );

		if ( $offset >= 0 && strpos( $src, $version_str, $offset ) !== FALSE ) :
			return substr( $src, 0, $offset );
		endif;

		return $src;

	}

endif;

add_filter( 'script_loader_src', 'wpstarter_remove_src_version' );
add_filter( 'style_loader_src', 'wpstarter_remove_src_version' );
