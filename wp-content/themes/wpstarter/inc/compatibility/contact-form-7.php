<?php
/**
 * Functions releated with Contact Form 7 plugin.
 */

/**
 * Remove 'p' tags from Contact Form 7 output.
 */
add_filter( 'wpcf7_autop_or_not', '__return_false' );

/**
 * Deregister Contact Form 7 scripts and styles.
 */
add_action( 'wp_enqueue_scripts', function() {

	if ( ! is_admin() ) :
		wp_dequeue_script( 'contact-form-7' );
		wp_dequeue_style( 'contact-form-7' );
	endif;

} );
