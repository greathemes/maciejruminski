<?php
/**
 * Portfolio custom post type.
 */

function wpstarter_register_portfolio_post_type() {

		$labels = [
			'name'               => esc_html__( 'Portfolio', 'TRANSLATE' ),
			'singular_name'      => esc_html__( 'Project', 'TRANSLATE' ),
			'add_new'            => esc_html_x( 'Add New Project', '${4:Name}', 'TRANSLATE' ),
			'add_new_item'       => esc_html__( 'Add New Project', 'TRANSLATE}' ),
			'edit_item'          => esc_html__( 'Edit Project', 'TRANSLATE' ),
			'new_item'           => esc_html__( 'New Project', 'TRANSLATE' ),
			'view_item'          => esc_html__( 'View Project', 'TRANSLATE' ),
			'search_items'       => esc_html__( 'Search Projects', 'TRANSLATE' ),
			'not_found'          => esc_html__( 'No Projects Found', 'TRANSLATE' ),
			'not_found_in_trash' => esc_html__( 'No Projects Found In Trash', 'TRANSLATE' ),
			'parent_item_colon'  => esc_html__( 'Parent Project:', 'TRANSLATE' ),
			'menu_name'          => esc_html__( 'Portfolio', 'TRANSLATE' ),
		];

		$args = array( 
			'labels'              => $labels,
			'hierarchical'        => true,
			'description'         => 'description',
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'has_archive'         => true,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => true,
			'capability_type'     => 'post', 
			'supports'            => [
				'title',
				'excerpt',
				'editor',
				'thumbnail',
			],
		);

		register_post_type( 'portfolio', $args );
}

add_action( 'init', 'wpstarter_register_portfolio_post_type' );
