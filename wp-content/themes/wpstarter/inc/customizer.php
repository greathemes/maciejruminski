<?php
/**
 * Customizer settings.
 */

function wpstarter_customizer( $wp_customize ) {

	// Add Section.
	$wp_customize->add_section( 'wpstarter', [
		'title' => esc_html__( 'Opcje', 'TRANSLATE' ), 
	] );

	// Add Settings.
	$wp_customize->add_setting( 'wpstarter_hero_img', [
		'transport' => 'refresh',
	] );

	$wp_customize->add_setting( 'wpstarter_cv', [
		'transport' => 'refresh',
	] );

	$wp_customize->add_setting( 'wpstarter_cv_page', [
		'capability' => 'edit_theme_options',
	] );

	$wp_customize->add_setting( 'wpstarter_cv_img', [
		'transport' => 'refresh',
	] );

	// Add Controls.
	$wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'wpstarter_hero_img', [
		'label'    => esc_html__( 'Zdjęcie dla górnej sekcji', 'TRANSLATE' ),
		'section'  => 'wpstarter',
		'settings' => 'wpstarter_hero_img',
	] ) );

	$wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'wpstarter_cv', [
		'label'    => esc_html__( 'Curriculum Vitae', 'TRANSLATE' ),
		'section'  => 'wpstarter',
		'settings' => 'wpstarter_cv',
	] ) );

	$wp_customize->add_control( 'wpstarter_cv_page', [
		'label'   => esc_html__( 'Strona z Curriculum Vitate', 'TRANSLATE' ),
		'type'    => 'dropdown-pages',
		'section' => 'wpstarter',
	] );

	$wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'wpstarter_cv_img', [
		'label'    => esc_html__( 'Zdjęcie do Curriculum Vitae', 'TRANSLATE' ),
		'section'  => 'wpstarter',
		'settings' => 'wpstarter_cv_img',
	] ) );

}

add_action( 'customize_register', 'wpstarter_customizer' );
