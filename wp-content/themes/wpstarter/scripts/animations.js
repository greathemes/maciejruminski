/**
 * Animation observer.
 */

document.addEventListener( 'DOMContentLoaded', function() {

const itemsToAnimate = Array.from( document.querySelectorAll( '.wait-for-animation' ) );

	// Do this only if IntersectionObserver is supported.
	if ( 'IntersectionObserver' in window ) {

		let itemsObserver = new IntersectionObserver( function( entries, observer ) {

				// Loop through IntersectionObserverEntry objects.
				entries.forEach( function( entry ) {

					// Do these if the target intersects with the root.
					if ( entry.isIntersecting ) {

						let
						item               = entry.target,
						classes            = [ 'wait-for-animation', 'wait-for-animation-top', 'wait-for-animation-bottom', 'wait-for-animation-right', 'wait-for-animation-left' ],
						transitionDelay    = parseFloat( window.getComputedStyle( item ).transitionDelay ) * 1000,
						transitionDuration = parseFloat( window.getComputedStyle( item ).transitionDuration ) * 1000;

						item.classList.remove( ...classes );
						itemsObserver.unobserve( item );

						setTimeout( function() {
							item.classList.remove( 'animation-delay' );
						}, transitionDelay + transitionDuration + 500 );

					}

				} );

		} );

		 // Loop through and observe each item.
		itemsToAnimate.forEach( function( item ) {
			item.classList.add( 'animation-delay' );
			itemsObserver.observe( item );
		} );

	}

} );