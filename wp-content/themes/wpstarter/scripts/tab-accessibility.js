/**
* After clicking on tab key add class to body.
*
* https://medium.com/hackernoon/removing-that-ugly-focus-ring-and-keeping-it-too-6c8727fefcd2
*/

/**
* Remove all animation classes, so show every element.
*/
function removeAnimationClasses( e ) {

	if ( e.keyCode === 9 ) {

		const
		itemsToAnimate = Array.from( document.querySelectorAll( '.wait-for-animation' ) ),
		classes        = [ 'wait-for-animation', 'wait-for-animation-top', 'wait-for-animation-bottom', 'wait-for-animation-right', 'wait-for-animation-left' ];

		// Remove all animation classes and transition delay of elements, user who use keyboard doesn't need animations.
		itemsToAnimate.forEach( function( el, i ) {
			el.classList.remove( ...classes );
			el.style.transitionDelay = '0s';
		} );

		window.removeEventListener( 'keydown', removeAnimationClasses );

	}

}

function handleFirstTab( e ) {

	if ( e.keyCode === 9 ) {

		document.body.classList.add( 'user-is-tabbing' );

		// Events.
		window.removeEventListener( 'keydown', handleFirstTab );
		window.addEventListener( 'mousedown', handleMouseDownOnce );

	}

}

function handleMouseDownOnce() {

	document.body.classList.remove( 'user-is-tabbing' );
	
	// Events.
	window.removeEventListener( 'mousedown', handleMouseDownOnce );
	window.addEventListener( 'keydown', handleFirstTab );

}

window.addEventListener( 'keydown', handleFirstTab );
window.addEventListener( 'keydown', removeAnimationClasses );
