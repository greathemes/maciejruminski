/**
 * Lazy loading for iamges.
 */

document.addEventListener( 'DOMContentLoaded', function() {

const lazyImages = Array.from( document.querySelectorAll( 'img.lazy' ) );

	// Do this only if IntersectionObserver is supported.
	if ( 'IntersectionObserver' in window ) {

		let lazyImageObserver = new IntersectionObserver( function( entries, observer ) {

				// Loop through IntersectionObserverEntry objects.
				entries.forEach( function( entry ) {

					// Do these if the target intersects with the root.
					if ( entry.isIntersecting ) {

						let
						lazyImage     = entry.target,
						lazyImageJpeg = lazyImage.previousElementSibling,
						lazyImageWebp = lazyImageJpeg.previousElementSibling;

						lazyImageJpeg.srcset = lazyImageJpeg.dataset.srcset;
						lazyImageWebp.srcset = lazyImageWebp.dataset.srcset;
						lazyImage.src = lazyImage.dataset.src;
						lazyImage.dataset.srcset ? lazyImage.srcset = lazyImage.dataset.srcset : '';
						lazyImage.classList.remove( 'lazy' );
						lazyImage.parentNode.style.height = '';
						lazyImageObserver.unobserve( lazyImage );

					}

				} );

		} );

		 // Loop through and observe each image.
		lazyImages.forEach( function( lazyImage ) {
			lazyImage.parentNode.style.height = lazyImage.parentNode.clientWidth / lazyImage.getAttribute( 'data-aspect-ratio' ) + 'px';
			lazyImageObserver.observe( lazyImage );
		} );

	}

} );
