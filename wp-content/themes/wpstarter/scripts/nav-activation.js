/**
* Activation of navigation links.
*/

const
desktopLinks = document.querySelectorAll( '.desktop-nav__link' ),
mobileLinks  = document.querySelectorAll( '.mobile-nav__link' ),
sections     = document.querySelectorAll( 'section.to-navigate' );

if ( sections.length ) {

	/**
	* https://gist.github.com/beaucharman/1f93fdd7c72860736643d1ab274fee1a
	*/

	function throttle( callback, wait, immediate = false ) {

		let timeout = null 
		
		return function() {

			const callNow = immediate && !timeout
			const next = () => callback.apply( this, arguments );
			
			clearTimeout( timeout );
			timeout = setTimeout( next, wait );

			if ( callNow ) {
				next()
			}

		}

	}

	const changeLinkState = throttle( ( arg, event ) => {

			let index = sections.length;

			while ( --index && window.scrollY < sections[index].offsetTop) {}
			
			// For desktop links.
			desktopLinks.forEach( ( link ) => link.classList.remove( 'is-active' ) );
			desktopLinks[ index ].classList.add( 'is-active' );

			// For mobile links.
			mobileLinks.forEach( ( link ) => link.classList.remove( 'is-active' ) );
			mobileLinks[ index ].classList.add( 'is-active' );

	}, 40, true );

	window.addEventListener( 'scroll', changeLinkState );
	changeLinkState();

}
