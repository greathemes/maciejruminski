/**
 * Table of contents:
 *
 * Nav - Show/Hide (navigation toggling)
 * Nav - activation functionality (Activation of appropriate link)
 * Portfolio - Read more (Read more after clicking on button)
 * Lazy Loading (Show images when visible on screen)
 * Contact Form 7 plugin (Compatibility)
 * Animations
 * Tab - accessibility
 */

import navToggling from './nav-toggling';
import navActivation from './nav-activation';
import portfolioReadMore from './portfolio-read-more';
import lazyLoading from './lazy-loading';
import contactForm7 from './compatibility/contact-form-7';
import animations from './animations';
import tabAccessibility from './tab-accessibility';
