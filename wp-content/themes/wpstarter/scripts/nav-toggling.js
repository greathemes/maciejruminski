/**
* Navigation toggling (between mobile and desktop).
*/

const
siteContainer = document.querySelector( '.site-container' );
mainHeader    = document.querySelector( '.main-header' );

if ( mainHeader ) {

	const
	mobileNav      = mainHeader.querySelector( '.mobile-nav' ),
	mobileNavBtn   = mainHeader.querySelector( '.mobile-nav__button' ),
	mobileNavLinks = Array.from( mainHeader.querySelectorAll( '.mobile-nav__link' ) ),
	desktopNavBtn  = mainHeader.querySelector( '.desktop-nav__button' );

	function toggleNavigation( e ) {

		/**
		* Show or hide navigation.
		*/

		if ( e.type === 'click' ) {

			if ( mobileNav.classList.contains( 'is-active' ) ) {
				mobileNavBtn.classList.toggle( 'is-hidden' );
				siteContainer.classList.remove( 'no-scroll' );
			} else {
				desktopNavBtn.classList.toggle( 'is-hidden' );
				setTimeout( function() {
					siteContainer.classList.add( 'no-scroll' );
				}, 2000 );
			}

			setTimeout( function() {
				mobileNav.classList.toggle( 'is-active' );
				setTimeout( function() {
					if ( mobileNav.classList.contains( 'is-active' ) ) {
							mobileNavBtn.classList.toggle( 'is-hidden' );
					} else {
						desktopNavBtn.classList.toggle( 'is-hidden' );
					}
				}, 750 );
			}, 1500 );

		} else if ( e.type === 'keydown' && e.keyCode === 27 ) {

			mobileNavBtn.classList.add( 'is-hidden' );

			setTimeout( function() {
				mobileNav.classList.remove( 'is-active' );
				siteContainer.classList.remove( 'no-scroll' );
				setTimeout( function() {
					desktopNavBtn.classList.remove( 'is-hidden' );
				}, 750 );
			}, 1500 );

		}


	}

	/**
	* Add toggleNavigation event for navigation buttons, mobile navigation links and ESC key.
	*/

	desktopNavBtn.addEventListener( 'click', toggleNavigation );
	mobileNavBtn.addEventListener( 'click', toggleNavigation );

	for ( const link of mobileNavLinks ) {
	  link.addEventListener( 'click', toggleNavigation );
	}

	document.addEventListener( 'keydown', toggleNavigation );

}
