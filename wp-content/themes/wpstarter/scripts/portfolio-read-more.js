/**
* Read more after clicking on button.
*/

const portfolioMoreButtons = document.querySelectorAll( '.front-page-portfolio__more' );

function showMore() {

	const
	descriptionsContainer = this.parentNode.querySelector( '.front-page-portfolio__descriptions' );
	descriptions          = descriptionsContainer.querySelectorAll( '.front-page-portfolio__description' );

	let height = 0;

	for ( const description of descriptions ) {
		height += description.clientHeight + parseInt( window.getComputedStyle( description ).marginTop );
	}

	this.style.transitionDelay = `0s`;
	this.style.height = 0;
	this.setAttribute( 'aria-expanded', 'true' );
	this.disabled = true;

	descriptionsContainer.style.height = `${height}px`;
	descriptionsContainer.setAttribute( 'aria-hidden', 'false' );

	setTimeout( () => {
		descriptionsContainer.style.height = '';
		descriptionsContainer.classList.remove( 'front-page-portfolio__descriptions--height' );
	}, 500 );

}

for ( const btn of portfolioMoreButtons ) {
	btn.addEventListener( 'click', showMore );
	btn.style.height = `${btn.clientHeight}px`;
}
