<?php
/**
 * This is the template that displays all of the <head> section.
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset='<?php bloginfo( 'charset' ); ?>'>
	<meta name='viewport' content='width=device-width, initial-scale=1'>
	<meta name='description' content='Maciej Rumiński - Junior Wordpress Developer. Zajmuję się tworzeniem stron www w oparciu o platformę WordPress.'>
	<link rel='profile' href='//gmpg.org/xfn/11'>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<?php if ( is_front_page() ) : ?>
		<header class='main-header'>
			<div class='main-header__container container'><?php get_template_part( 'template-parts/navigation' ); ?></div>
		</header>
	<?php endif; ?>

	<div class='site-container'>
