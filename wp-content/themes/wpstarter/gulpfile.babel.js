/**
 * Gulp file.
 */

import gulp from 'gulp';
import yargs from 'yargs';
import sass from 'gulp-sass';
import cleancss from 'gulp-clean-css';
import autoprefixer from 'gulp-autoprefixer';
import gulpif from 'gulp-if';
import sourcemaps from 'gulp-sourcemaps';
import webpack from 'webpack-stream';
import del from 'del';
import uglify from 'gulp-uglify-es';
import concat from 'gulp-concat';
import rename from 'gulp-rename';
import named from 'vinyl-named';
import browsersync from 'browser-sync';
import zip from 'gulp-zip';
import replace from 'gulp-replace';
import wpPot from 'gulp-wp-pot';
import info from './package.json';
import file from 'gulp-file';

const dev_name        = 'wpstarter',
capitalize_dev_name   = 'Wpstarter',
uppercase_dev_name    = 'WPSTARTER',
translate_string      = 'TRANSLATE',
theme_name            = info.name,
theme_name_dir        = '../' + theme_name,
capitalize_theme_name = theme_name.charAt( 0 ).toUpperCase() + theme_name.slice( 1 ),
uppercase_theme_name  = theme_name.toUpperCase(),
assets                = 'assets/',
styles_dir            = 'styles',
assets_js             = 'assets/scripts',
server                = browsersync.create(),
PRODUCTION            = yargs.argv.prod;

// Server.
export const serve = ( done ) => {

	server.init( { proxy: 'http://localhost:8888/' + theme_name } );
	done();

}

// Reload.
export const reload = ( done ) => {

	server.reload();
	done();

}

// Create .pot file.
export const pot = () => {

	return gulp.src( '**/*.php' )
	.pipe(
		wpPot(
			{
				domain: translate_string,
				package: theme_name
			}
		)
	)
	.pipe( gulp.dest( 'languages/' + dev_name + '.pot' ) )

}

// Remove production directory & .zip file.
export const clean = () => del( [ theme_name_dir, theme_name_dir + '.zip' ], { force: true } );

// Translation.
export const translate = ( to_translate ) => {

	return to_translate
	.pipe( replace( dev_name, theme_name ) )
	.pipe( replace( capitalize_dev_name, capitalize_theme_name ) )
	.pipe( replace( uppercase_dev_name, uppercase_theme_name ) )
	.pipe( replace( translate_string, theme_name ) );

}

// Styles.
export const styles = ( done ) => {

	return translate( gulp.src( styles_dir + '/styles.scss' ) )
		.pipe( gulpif( ! PRODUCTION, sourcemaps.init() ) )
		.pipe( sass().on( 'error', sass.logError ) )
		.pipe( autoprefixer(
			{
				browsers: 'last 2 versions, > 1%',
				cascade: false
			}
		) )
		.pipe( gulpif( ! PRODUCTION, sourcemaps.write() ) )
		.pipe( gulpif( PRODUCTION, cleancss( { compatibility: 'ie11' } ) ) )
		.pipe( rename( { suffix: '.min' } ) )
		.pipe( gulp.dest( theme_name_dir ) )
		done();

}

// Styles for logged in (front).
export const stylesLoggedIn = ( done ) => {

	return translate( gulp.src( styles_dir + '/styles-logged-in.scss' ) )
		.pipe( gulpif( ! PRODUCTION, sourcemaps.init() ) )
		.pipe( sass().on( 'error', sass.logError ) )
		.pipe( autoprefixer(
			{
				browsers: 'last 2 versions, > 1%',
				cascade: false
			}
		) )
		.pipe( gulpif( ! PRODUCTION, sourcemaps.write() ) )
		.pipe( gulpif( PRODUCTION, cleancss( { compatibility: 'ie11' } ) ) )
		.pipe( rename( { suffix: '.min' } ) )
		.pipe( gulp.dest( theme_name_dir ) )
		done();

}

// Scripts.
export const scripts = ( done ) => {

	return translate( gulp.src( 'scripts/*.js' ) )
		.pipe( webpack( require( './webpack.config.js' ) ) )
		.pipe( gulp.dest( theme_name_dir) );

}

// Scripts for logged in (front).
export const scriptsLoggedIn = ( done ) => {

	return translate( gulp.src( 'scripts/*.js' ) )
		.pipe( webpack( require( './webpack-logged-in.config.js' ) ) )
		.pipe( gulp.dest( theme_name_dir) );

}

// Views (php).
export const php = ( done ) => {

	return translate( gulp.src( '**/*.php' ) )
	.pipe(
		rename( ( opt ) => {
			opt.basename = opt.basename.replace( dev_name, theme_name );
			opt.basename = opt.basename.replace( capitalize_dev_name, capitalize_theme_name );
			return opt;
		} )
	)
	.pipe( gulp.dest( theme_name_dir ) );

}

export const files = ( done ) => {

	// Style.css file.
	translate( gulp.src( 'style.css' ) )
	.pipe( gulp.dest( theme_name_dir ) );

	// Lib directory.
	gulp.src( 'lib/**.*' )
	.pipe( gulp.dest( theme_name_dir + '/lib' ) );

	// Fonts directory.
	gulp.src( 'fonts/**.*' )
	.pipe( gulp.dest( theme_name_dir + '/fonts' ) );

	// Languages
	translate( gulp.src( [ 'languages/' + dev_name + '.pot' ] ) )
	.pipe(
		rename( ( opt ) => {
			opt.basename = opt.basename.replace( dev_name, theme_name );
			return opt;
		} )
	)
	.pipe( gulp.dest( theme_name_dir + '/languages' ) );

	done();

}

// Watch.
export const watch = () => {

	gulp.watch( 'styles/**/*.scss', gulp.series( styles, stylesLoggedIn, reload ) );
	gulp.watch( 'scripts/**/*.js', gulp.series( scripts, scriptsLoggedIn, reload ) );
	gulp.watch( '**/*.php', gulp.series( php, reload ) );

}

// Tasks.
export const dev = gulp.series( clean, gulp.parallel( styles, stylesLoggedIn, scripts, scriptsLoggedIn, php, files ), serve, watch );
export const build = gulp.series( clean, pot, gulp.parallel( styles, stylesLoggedIn, scripts, scriptsLoggedIn, php, files ) );
export const production = gulp.series( build );

export default dev;
