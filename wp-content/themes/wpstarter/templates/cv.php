<?php /* Template Name: Curriculum Vitae */

get_header(); ?>

<section class='cv'>

	<div class='cv__container container'>

		<div class='cv__links'>
			<a class='cv__link button wait-for-animation wait-for-animation-top' href='<?php echo esc_url( get_bloginfo( 'url' ) ); ?>'><?php esc_html_e( 'Wróć do strony głównej', 'TRANSLATE' ) ?></a>
			<a class='cv__link sd_button wait-for-animation wait-for-animation-bottom' href='<?php echo esc_url( wp_get_attachment_url( get_theme_mod( 'wpstarter_cv' ) ) ); ?>' target='_blank' title='<?php esc_attr_e( 'Zobacz moje Curriculum Vitae w formacie PDF.', 'TRANSLATE' ) ?>'><?php esc_html_e( 'CV w PDF', 'TRANSLATE' ) ?></a>
		</div>

		<header class='cv__header wait-for-animation'>
			<h1 class='cv__heading wait-for-animation wait-for-animation-right'><?php esc_html_e( 'Curriculum Vitae', 'TRANSLATE' ) ?></h1>
		</header>

		<div class='cv_innerContainer'>

			<div class='cv__about wait-for-animation'>

				<?php $img = wp_get_attachment_image_src( get_theme_mod( 'wpstarter_cv_img' ), 'full' ); ?>
				<picture class='cv__imageContainer wait-for-animation wait-for-animation-left'>
					<!-- WEBP -->
					<source
						data-srcset='<?php echo esc_url( "{$img[0]}.webp" ); ?>'
						srcset='data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
						type='image/webp'
					>
					<!-- JPEG -->
					<source
						data-srcset='<?php echo esc_url( $img[0] ); ?>'
						srcset='data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
						type='image/jpeg'
					>
					<img
						class='cv__image lazy'
						data-src='<?php echo esc_url( $img[0] ); ?>'
						src='data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
						data-aspect-ratio='<?php echo esc_attr( $img[1] / $img[2] ); ?>'
						alt='<?php echo esc_attr( get_post_meta( get_post_thumbnail_id( get_theme_mod( 'wpstarter_cv_img' ) ), '_wp_attachment_image_alt', true ) ); ?>'
					>
				</picture>

				<p class='cv__name wait-for-animation wait-for-animation-left'>Maciej Rumiński</p>
				<p class='cv__occupation wait-for-animation wait-for-animation-left'><?php esc_html_e( 'Junior WordPress Developer', 'TRANSLATE' ); ?></p>
				<h2 class='cv__aboutHeading wait-for-animation wait-for-animation-left'><?php esc_html_e( 'Kontakt', 'TRANSLATE' ); ?></h2>
				<address class='cv__contact wait-for-animation wait-for-animation-left'>
					<a class='cv__contactEmail wait-for-animation wait-for-animation-left' href='mailto:mac-rum@wp.pl'>
						<?php
						printf(
							// translators: %1$s: Start of the HTML span tag.
							// translators: %2$s: End of the HTML span tag.
							esc_html__( '%1$sEmail:%2$s mac-rum@wp.pl', 'TRANSLATE' ),
							'<span class="cv__contactEmail--bold">',
							'</span>'
						);
						?>
					</a>
					<a class='cv__contactTel wait-for-animation wait-for-animation-left' href='tel:726-939-908'>
						<?php
						printf(
							// translators: %1$s: Start of the HTML span tag.
							// translators: %2$s: End of the HTML span tag.
							esc_html__( '%1$sTel:%2$s 726-939-908', 'TRANSLATE' ),
							'<span class="cv__contactTel--bold">',
							'</span>'
						);
						?>
					</a>
					<a class='cv__contactPortfolio wait-for-animation wait-for-animation-left' href='<?php echo esc_url( get_bloginfo( 'url' ) ); ?>'>
						<?php
						printf(
							// translators: %1$s: Start of the HTML span tag.
							// translators: %2$s: End of the HTML span tag.
							esc_html__( '%1$sPortfolio:%2$s maciejruminski.com', 'TRANSLATE' ),
							'<span class="cv__contactPortfolio--bold">',
							'</span>'
						);
						?>
					</a>
				</address>
				<h2 class='cv__aboutHeading wait-for-animation wait-for-animation-left'><?php esc_html_e( 'Krótko o mnie', 'TRANSLATE' ); ?></h2>
				<p class='cv__aboutContent cv__aboutContent--line wait-for-animation wait-for-animation-left'><?php esc_html_e( 'Urodziłem się w Grudziądzu w 1991 roku. W latach 2006-2010 ukończyłem technikum elektryczne (ZST), jednak kierunek, który obrałem związany jest z tworzeniem stron internetowych. To jest to co mnie interesuje i w czym chciałbym się dalej rozwijać. Jestem sumienny, pozytywny i wg. moich szefów korzystnie radziłem sobie w poprzednich pracach.', 'TRANSLATE' ); ?></p>
				<h2 class='cv__aboutHeading wait-for-animation wait-for-animation-left'><?php esc_html_e( 'Zainteresowania', 'TRANSLATE' ); ?></h2>
				<p class='cv__aboutContent wait-for-animation wait-for-animation-left'><?php esc_html_e( 'Prywatnie interesuję się sportem, lubię pograć w gry RPG, piję Yerba Mate i szukam kolejnej dawki adrenaliny mając za sobą skok ze spadochronem i na bungee.', 'TRANSLATE' ); ?></p>
			</div>

			<div class='cv__experience wait-for-animation'>
				<h2 class='cv__experienceHeading wait-for-animation wait-for-animation-right'><?php esc_html_e( 'Doświadczenie', 'TRANSLATE' ); ?></h2>

				<ul class='cv__experienceList'>

					<li class='cv__experienceItem cv__experienceItem--line wait-for-animation wait-for-animation-right'>
						<h3 class='cv__experienceTitle wait-for-animation wait-for-animation-right'><?php esc_html_e( 'Junior Theme Developer (Praca w domu)', 'TRANSLATE' ); ?></h3>
						<span class='cv__experienceDate wait-for-animation wait-for-animation-right'><?php esc_html_e( '01.10.2018 - Obecnie', 'TRANSLATE' ); ?></span>
						<p class='cv__experienceDescription wait-for-animation wait-for-animation-right'><?php esc_html_e( 'Ponownie praca z WordPressem, tworzenie motywów i próba sprzedaży na stronach typu Mojo, Creative czy Envato Market, niestety nieudana ze względu na brak doświadczenia z innych dziedzin, które okazały się niezbędne do osiągnięcia sukcesu. Korzystałem z technologii poznanych w pierwszej pracy, natomiast w celu tworzenia opcji w panelu wykorzystywałem Customizer API w połączeniu z wtyczką Kirki. Dodatkowo sporo nauki/pracy z optymalizacją (Transient API, lazy images, cache, itd.)', 'TRANSLATE' ); ?></p>
					</li>

					<li class='cv__experienceItem cv__experienceItem--line wait-for-animation wait-for-animation-right'>
						<h3 class='cv__experienceTitle wait-for-animation wait-for-animation-right'><?php esc_html_e( 'Magazynier (Amazon - Milton Keynes, Anglia)', 'TRANSLATE' ); ?></h3>
						<span class='cv__experienceDate wait-for-animation wait-for-animation-right'><?php esc_html_e( '03.07.2018 - 01.10.2018', 'TRANSLATE' ); ?></span>
						<p class='cv__experienceDescription wait-for-animation wait-for-animation-right'><?php esc_html_e( 'Zakres obowiązków na tym stanowisku nie ma związku z pracą w IT, natomiast jestem zdania, że praca za granicą zawsze jest warta wspomnienia. Kilka miesięcy prawie codziennej pracy, w której poradziłem sobie ze stresami towarzyszącymi podczas wyjazdu do innego kraju.', 'TRANSLATE' ); ?></p>
					</li>

					<li class='cv__experienceItem cv__experienceItem--line wait-for-animation wait-for-animation-right'>
						<h3 class='cv__experienceTitle wait-for-animation wait-for-animation-right'><?php esc_html_e( 'Junior WordPress Developer (Red Frog Studio - Wrocław)', 'TRANSLATE' ); ?></h3>
						<span class='cv__experienceDate wait-for-animation wait-for-animation-right'><?php esc_html_e( '02.05.2017 - 02.06.2018', 'TRANSLATE' ); ?></span>
						<p class='cv__experienceDescription wait-for-animation wait-for-animation-right'><?php esc_html_e( 'Na tym stanowisku byłem odpowiedzialny za tworzenie stron, edytowanie czy naprawianie błędów dla klientów w oparciu o WordPress. Korzystałem z HTML, CSS/SCSS, PHP (na potrzeby WordPressa), JavaScript (głównie jQuery), GULP, GIT, Photoshop. Główną wtyczką z jakiej korzystałem w niemalże każdym projekcie był Advanced Custom Fields. Niektóre strony tworzyłem również w oparciu o wtyczkę WPBakery Page Builder. Często jednym z wyzwań było pixel perfect.', 'TRANSLATE' ); ?></p>
					</li>

					<li class='cv__experienceItem cv__experienceItem--line wait-for-animation wait-for-animation-right'>
						<h3 class='cv__experienceTitle wait-for-animation wait-for-animation-right'><?php esc_html_e( 'Drukarz/Grafik (KMD - Grudziądz)', 'TRANSLATE' ); ?></h3>
						<span class='cv__experienceDate wait-for-animation wait-for-animation-right'><?php esc_html_e( '02.03.2015 - 29.03.2017', 'TRANSLATE' ); ?></span>
						<p class='cv__experienceDescription wait-for-animation wait-for-animation-right'><?php esc_html_e( 'Moja pierwsza praca biurowa. Co prawda zakres obowiązków nie miał związku z tworzeniem stron WWW, ale nauczyłem się tutaj podstaw Photoshopa. Grafikiem artystą nie jestem, wykonywałem jedynie projekty na potrzeby druku, proste banery, ulotki czy wizytówki, ale wyrobiłem sobie dzięki temu pewne poczucie estetyki co przydaje się w pracy Web Developera.', 'TRANSLATE' ); ?></p>
					</li>

				</ul>

				<h2 class='cv__experienceHeading wait-for-animation wait-for-animation-right'><?php esc_html_e( 'Umiejętności miękkie/twarde', 'TRANSLATE' ); ?></h2>

				<ul class='cv__experienceList cv__experienceList--skills'>
					<li class='cv__experienceItem cv__experienceItem--skill wait-for-animation wait-for-animation-right'><?php esc_html_e( 'HTML, CSS/SCSS, podstawy PHP, WordPress (funkcje, filtry, akcje, niektóre wtyczki jak np. ACF, WPML, CF7), GULP, JavaScript/jQuery (podstawy ES6), podstawy GIT, podstawy SEO, podstawy dostępności oraz podstawowe kwestie związane z optymalizacją.', 'TRANSLATE' ); ?></li>
					<li class='cv__experienceItem cv__experienceItem--skill wait-for-animation wait-for-animation-right'><?php esc_html_e( 'Podstawy Photoshop oraz Affinity Designer, umiejętność odróżnienia estetycznego designu od przestarzałego oraz podejście nastawione na to, aby wszystko było równe i symetryczne.', 'TRANSLATE' ); ?></li>
					<li class='cv__experienceItem cv__experienceItem--skill wait-for-animation wait-for-animation-right'><?php esc_html_e( 'Angielski pozwalający korzystać z dokumentacji, Google, Stack Overflow, itd.', 'TRANSLATE' ); ?></li>
					<li class='cv__experienceItem cv__experienceItem--skill wait-for-animation wait-for-animation-right'><?php esc_html_e( 'Sumienność, wytrwałość, chęć do nauki, koncentracja na wykonywanych obowiązkach, łatwość nawiązywania kontaktów, pozytywne usposobienie, korzystne doświadczenia w pracy z ludźmi.', 'TRANSLATE' ); ?></li>
				</ul>

			</div>

		</div>

		<div class='cv__links'>
			<a class='cv__link button wait-for-animation wait-for-animation-top' href='<?php echo esc_url( get_bloginfo( 'url' ) ); ?>'><?php esc_html_e( 'Wróć do strony głównej', 'TRANSLATE' ) ?></a>
			<a class='cv__link sd_button wait-for-animation wait-for-animation-bottom' href='<?php echo esc_url( wp_get_attachment_url( get_theme_mod( 'wpstarter_cv' ) ) ); ?>' target='_blank' title='<?php esc_attr_e( 'Zobacz moje Curriculum Vitae w formacie PDF.', 'TRANSLATE' ) ?>'><?php esc_html_e( 'CV w PDF', 'TRANSLATE' ) ?></a>
		</div>

	</div>

</section>

<?php get_footer();
