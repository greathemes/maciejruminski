# Copyright (C) 2019 maciejruminski
# This file is distributed under the same license as the maciejruminski package.
msgid ""
msgstr ""
"Project-Id-Version: maciejruminski\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"
"X-Poedit-SourceCharset: UTF-8\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. translators: %1$s: HTML Copyright sign.
#: footer.php:18
msgid "Copyright %1$s %2$s maciejruminski.pl"
msgstr ""

#. translators: %1$s: Start of the HTML span tag.
#. translators: %1$s: Start of the HTML span tag.
#: footer.php:31, templates/cv.php:65
msgid "%1$sTel:%2$s 726-939-908"
msgstr ""

#. translators: %1$s: Start of the HTML span tag.
#. translators: %1$s: Start of the HTML span tag.
#: footer.php:42, templates/cv.php:54
msgid "%1$sEmail:%2$s mac-rum@wp.pl"
msgstr ""

#: inc/class-wpstarter-walker-primary.php:106
msgid "Pokaż menu rozwijane"
msgstr ""

#: inc/cpt.php:9, inc/cpt.php:20
msgid "Portfolio"
msgstr ""

#: inc/cpt.php:10
msgid "Project"
msgstr ""

#: inc/cpt.php:11
msgctxt "${4:Name}"
msgid "Add New Project"
msgstr ""

#: inc/cpt.php:13
msgid "Edit Project"
msgstr ""

#: inc/cpt.php:14
msgid "New Project"
msgstr ""

#: inc/cpt.php:15
msgid "View Project"
msgstr ""

#: inc/cpt.php:16
msgid "Search Projects"
msgstr ""

#: inc/cpt.php:17
msgid "No Projects Found"
msgstr ""

#: inc/cpt.php:18
msgid "No Projects Found In Trash"
msgstr ""

#: inc/cpt.php:19
msgid "Parent Project:"
msgstr ""

#: inc/customizer.php:10
msgid "Opcje"
msgstr ""

#: inc/customizer.php:32
msgid "Zdjęcie dla górnej sekcji"
msgstr ""

#: inc/customizer.php:38, templates/cv.php:15
msgid "Curriculum Vitae"
msgstr ""

#: inc/customizer.php:44
msgid "Strona z Curriculum Vitate"
msgstr ""

#: inc/customizer.php:50
msgid "Zdjęcie do Curriculum Vitae"
msgstr ""

#: inc/setup.php:47
msgid "Primary Navigation"
msgstr ""

#: template-parts/front-page-about.php:16
msgid "Kim jestem?"
msgstr ""

#. translators: %1$s: Start of the HTML span tag.
#: template-parts/front-page-about.php:22
msgid "Trochę o  mnie%1$s.%2$s"
msgstr ""

#: template-parts/front-page-about.php:33
msgid "Co potrafię?"
msgstr ""

#: template-parts/front-page-about.php:34
msgid "Moje umiejętności są dość typowe dla osoby na poziomie Juniora, który głównie tworzy korzystając z WordPressa."
msgstr ""

#: template-parts/front-page-about.php:35
msgid "W swoich projektach korzystam z podstawowego PHP (na poziomie WP), HTML, CSS/SCSS, GULP, JavaScript/jQuery. Korzystam również z GIT, ale są to jedynie podstawowe komendy wpisywane z poziomu terminala (diff, status, add, push, itd.)."
msgstr ""

#: template-parts/front-page-about.php:36
msgid "Odnośnie WordPressa, rozumiem działanie filtrów i akcji, wiem jak korzystać z WordPress Codex, rozumiem co to jest \"plugin territory\". Pracowałem też z Custom Post Types oraz Taxonomy. Odnośnie pluginów korzystałem np. z Kirki, ACF czy WPML. Mam niestety obecnie bardzo niewielkie doświadczenie z WooCommerce."
msgstr ""

#: template-parts/front-page-about.php:37
msgid "Jeśli chodzi o kwestie związane z projektowaniem to myślę, że mam jako takie poczucie estetyki ale z całą pewnością nie jestem grafikiem i zdecydowanie wolę dostać gotowy projekt. Znam podstawy Photoshop czy Affinity Designer."
msgstr ""

#: template-parts/front-page-about.php:38
msgid "Staram się również zwiększać swoją wiedzę nt. optymalizowania strony (kompresja zdjęć, lazy loading, itd.) na podstawie raportu ze strony GTmetrix czy PageSpeed Insights. Nie znoszę wolnych stron i staram się aby moje witryny działały możliwie szybko. Obecnie sporo czytam również o accessibility, treść powinna być dostępna dla każdego."
msgstr ""

#: template-parts/front-page-about.php:39
msgid "Ostatnia kwestia to angielski, który u mnie sprowadza się do czytania. Pisanie, mówienie i słuchanie wypada niestety gorzej o czym przekonałem się w Anglii (co prawda pracuję nad tym, ale nie jest to niestety umiejętność, którą da się wyraźnie poprawić w krótkim czasie)."
msgstr ""

#: template-parts/front-page-about.php:44
msgid "Dlaczego programowanie?"
msgstr ""

#: template-parts/front-page-about.php:45
msgid "Nie będę tutaj pisał o pasji, której początki miały miejsce przed narodzinami, ponieważ po pierwsze, pisanie tego wydaje mi się już bardzo oklepane, a po drugie, nic takiego nie miało miejsca, moje wejście w świat kodu to zwykły (ale przyjemny) przypadek :)"
msgstr ""

#: template-parts/front-page-about.php:46
msgid "Pewnego dnia wrzuciłem na forum graficzne projekt strony internetowej i jeden z użytkowników napisał, że zrobienie do tego sensownego HTMLa i CSSa zajmie wieki (i miał rację, nie znałem wtedy jeszcze żadnych zasad tworzenia stron, projekt był po prostu kiepski)."
msgstr ""

#: template-parts/front-page-about.php:47
msgid "Jako, że nie miałem pojęcia czym jest HTML, wpisałem ową frazę w Google, wklepałem pierwsze \"Hello World\" i zwyczajnie zaciekawiłem się tematem ruszając dalej i pozostając już na tej ścieżce. Zacząłem od absolutnego zera, nie uczestniczyłem w Bootcampach, płatnych szoleniach i tego typu rzeczach, jestem typowym samoukiem, wymyślam coś i z pomocą internetu zaczynam \"grzebać\"."
msgstr ""

#: template-parts/front-page-about.php:48
msgid "Dlaczego to robię? Tworzenie schludnych, szybkich, fajnych stron daje mi po prostu frajdę i satysfakcję."
msgstr ""

#: template-parts/front-page-about.php:50
msgid "Króciutko o mnie."
msgstr ""

#: template-parts/front-page-about.php:51
msgid "Raczej łatwo nawiązuję kontakty, jestem chętny do pracy i zmotywowany. Potrafię utrzymać koncentrację na zadaniu, nie myśląc w tym czasie o Facebookach (co nie znaczy, że jestem ponurakiem). Wg. byłych kolegów z pracy, siedzenie ze mną w biurze to prędzej przyjemność niż katusze :)"
msgstr ""

#: template-parts/front-page-about.php:58
msgid "Przejdź do strony z moim Curriculum Vitae."
msgstr ""

#: template-parts/front-page-about.php:58
msgid "Zobacz moje CV"
msgstr ""

#: template-parts/front-page-about.php:59, templates/cv.php:11, templates/cv.php:135
msgid "Zobacz moje Curriculum Vitae w formacie PDF."
msgstr ""

#: template-parts/front-page-about.php:59, templates/cv.php:11, templates/cv.php:135
msgid "CV w PDF"
msgstr ""

#: template-parts/front-page-contact.php:14
msgid "Masz pytanie? Śmiało, napisz"
msgstr ""

#. translators: %1$s: Start of the HTML span tag.
#: template-parts/front-page-contact.php:20
msgid "Kontakt%1$s.%2$s"
msgstr ""

#. translators: %1$s: Start of the HTML span tag.
#: template-parts/front-page-hero.php:44
msgid "%1$sMoja profesja to%2$s Junior WordPress Developer"
msgstr ""

#. translators: %1$s: Start of the HTML span tag.
#: template-parts/front-page-hero.php:55
msgid "Hej, nazywam się Maciej i witam Cię na moim portfolio%1$s!%2$s"
msgstr ""

#. translators: %1$s: Start of the HTML span tag.
#: template-parts/front-page-hero.php:66
msgid "Tworzę strony internetowe w oparciu o CMS WordPress i %1$sobecnie szukam stałej pracy/stażu na terenie Krakowa%2$s, aby zwiększyć swoje umiejętności pracując u boku bardziej doświadczonych kolegów/koleżanek."
msgstr ""

#: template-parts/front-page-hero.php:74
msgid "Przejdź do sekcji portfolio"
msgstr ""

#: template-parts/front-page-hero.php:74
msgid "Zobacz projekty"
msgstr ""

#: template-parts/front-page-hero.php:75
msgid "Przejdź do sekcji o mnie"
msgstr ""

#: template-parts/front-page-hero.php:75
msgid "Poznaj mnie bliżej"
msgstr ""

#: template-parts/front-page-portfolio.php:14
msgid "Kilka moich projektów."
msgstr ""

#. translators: %1$s: Start of the HTML span tag.
#: template-parts/front-page-portfolio.php:20
msgid "Portfolio%1$s.%2$s"
msgstr ""

#. translators: %2$s: End of the HTML span tag.
#: template-parts/front-page-portfolio.php:86
msgid "%1$sNazwa projektu:%2$s %3$s"
msgstr ""

#. translators: %2$s: End of the HTML span tag.
#: template-parts/front-page-portfolio.php:100
msgid "Czytaj więcej %1$sna temat projektu %3$s%2$s"
msgstr ""

#: template-parts/front-page-portfolio.php:120
msgid "Technologie/narzędzia:"
msgstr ""

#: template-parts/front-page-portfolio.php:135
msgid "Linki:"
msgstr ""

#. translators: %s: The title of the project.
#: template-parts/front-page-portfolio.php:146
msgid "Zobacz projekt %s na żywo."
msgstr ""

#: template-parts/front-page-portfolio.php:150
msgid "Demo"
msgstr ""

#. translators: %s: The title of the project.
#: template-parts/front-page-portfolio.php:163
msgid "Zobacz repozytorium projektu %s."
msgstr ""

#: template-parts/front-page-portfolio.php:167
msgid "Repozytorium"
msgstr ""

#: template-parts/navigation.php:30
msgid "Pokaż nawigację"
msgstr ""

#: template-parts/navigation.php:50
msgid "Schowaj nawigację"
msgstr ""

#: templates/cv.php:10, templates/cv.php:134
msgid "Wróć do strony głównej"
msgstr ""

#: templates/cv.php:46
msgid "Junior WordPress Developer"
msgstr ""

#: templates/cv.php:47
msgid "Kontakt"
msgstr ""

#. translators: %1$s: Start of the HTML span tag.
#: templates/cv.php:76
msgid "%1$sPortfolio:%2$s maciejruminski.com"
msgstr ""

#: templates/cv.php:83
msgid "Krótko o mnie"
msgstr ""

#: templates/cv.php:84
msgid "Urodziłem się w Grudziądzu w 1991 roku. W latach 2006-2010 ukończyłem technikum elektryczne (ZST), jednak kierunek, który obrałem związany jest z tworzeniem stron internetowych. To jest to co mnie interesuje i w czym chciałbym się dalej rozwijać. Jestem sumienny, pozytywny i wg. moich szefów korzystnie radziłem sobie w poprzednich pracach."
msgstr ""

#: templates/cv.php:85
msgid "Zainteresowania"
msgstr ""

#: templates/cv.php:86
msgid "Prywatnie interesuję się sportem, lubię pograć w gry RPG, piję Yerba Mate i szukam kolejnej dawki adrenaliny mając za sobą skok ze spadochronem i na bungee."
msgstr ""

#: templates/cv.php:90
msgid "Doświadczenie"
msgstr ""

#: templates/cv.php:95
msgid "Junior Theme Developer (Praca w domu)"
msgstr ""

#: templates/cv.php:96
msgid "01.10.2018 - Obecnie"
msgstr ""

#: templates/cv.php:97
msgid "Ponownie praca z WordPressem, tworzenie motywów i próba sprzedaży na stronach typu Mojo, Creative czy Envato Market, niestety nieudana ze względu na brak doświadczenia z innych dziedzin, które okazały się niezbędne do osiągnięcia sukcesu. Korzystałem z technologii poznanych w pierwszej pracy, natomiast w celu tworzenia opcji w panelu wykorzystywałem Customizer API w połączeniu z wtyczką Kirki. Dodatkowo sporo nauki/pracy z optymalizacją (Transient API, lazy images, cache, itd.)"
msgstr ""

#: templates/cv.php:101
msgid "Magazynier (Amazon - Milton Keynes, Anglia)"
msgstr ""

#: templates/cv.php:102
msgid "03.07.2018 - 01.10.2018"
msgstr ""

#: templates/cv.php:103
msgid "Zakres obowiązków na tym stanowisku nie ma związku z pracą w IT, natomiast jestem zdania, że praca za granicą zawsze jest warta wspomnienia. Kilka miesięcy prawie codziennej pracy, w której poradziłem sobie ze stresami towarzyszącymi podczas wyjazdu do innego kraju."
msgstr ""

#: templates/cv.php:107
msgid "Junior WordPress Developer (Red Frog Studio - Wrocław)"
msgstr ""

#: templates/cv.php:108
msgid "02.05.2017 - 02.06.2018"
msgstr ""

#: templates/cv.php:109
msgid "Na tym stanowisku byłem odpowiedzialny za tworzenie stron, edytowanie czy naprawianie błędów dla klientów w oparciu o WordPress. Korzystałem z HTML, CSS/SCSS, PHP (na potrzeby WordPressa), JavaScript (głównie jQuery), GULP, GIT, Photoshop. Główną wtyczką z jakiej korzystałem w niemalże każdym projekcie był Advanced Custom Fields. Niektóre strony tworzyłem również w oparciu o wtyczkę WPBakery Page Builder. Często jednym z wyzwań było pixel perfect."
msgstr ""

#: templates/cv.php:113
msgid "Drukarz/Grafik (KMD - Grudziądz)"
msgstr ""

#: templates/cv.php:114
msgid "02.03.2015 - 29.03.2017"
msgstr ""

#: templates/cv.php:115
msgid "Moja pierwsza praca biurowa. Co prawda zakres obowiązków nie miał związku z tworzeniem stron WWW, ale nauczyłem się tutaj podstaw Photoshopa. Grafikiem artystą nie jestem, wykonywałem jedynie projekty na potrzeby druku, proste banery, ulotki czy wizytówki, ale wyrobiłem sobie dzięki temu pewne poczucie estetyki co przydaje się w pracy Web Developera."
msgstr ""

#: templates/cv.php:120
msgid "Umiejętności miękkie/twarde"
msgstr ""

#: templates/cv.php:123
msgid "HTML, CSS/SCSS, podstawy PHP, WordPress (funkcje, filtry, akcje, niektóre wtyczki jak np. ACF, WPML, CF7), GULP, JavaScript/jQuery (podstawy ES6), podstawy GIT, podstawy SEO, podstawy dostępności oraz podstawowe kwestie związane z optymalizacją."
msgstr ""

#: templates/cv.php:124
msgid "Podstawy Photoshop oraz Affinity Designer, umiejętność odróżnienia estetycznego designu od przestarzałego oraz podejście nastawione na to, aby wszystko było równe i symetryczne."
msgstr ""

#: templates/cv.php:125
msgid "Angielski pozwalający korzystać z dokumentacji, Google, Stack Overflow, itd."
msgstr ""

#: templates/cv.php:126
msgid "Sumienność, wytrwałość, chęć do nauki, koncentracja na wykonywanych obowiązkach, łatwość nawiązywania kontaktów, pozytywne usposobienie, korzystne doświadczenia w pracy z ludźmi."
msgstr ""
