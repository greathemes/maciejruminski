<?php
/**
 * Functions and definitions.
 */

// Constants.
define( 'WPSTARTER_THEME_DIR', trailingslashit( get_template_directory() ) );
define( 'WPSTARTER_THEME_INCLUDES', trailingslashit( get_template_directory() . '/inc' ) );
define( 'WPSTARTER_THEME_COMPATIBILITY', trailingslashit( get_template_directory() . '/inc/compatibility' ) );
define( 'WPSTARTER_THEME_URI', trailingslashit( esc_url( get_template_directory_uri() ) ) );

// Setup.
require_once WPSTARTER_THEME_INCLUDES . 'setup.php';
require_once WPSTARTER_THEME_INCLUDES . 'cpt.php';

// Walker.
require_once WPSTARTER_THEME_INCLUDES . 'class-wpstarter-walker-primary.php';

// Customizer.
require_once WPSTARTER_THEME_INCLUDES . 'customizer.php';

// Custom functions.
require_once WPSTARTER_THEME_INCLUDES . 'custom-functions.php';

// Compatibility.
require_once WPSTARTER_THEME_COMPATIBILITY . 'contact-form-7.php';
