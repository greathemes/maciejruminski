<?php
/**
 * The template for displaying the footer.
 */
?>

</div>

<footer class='main-footer'>

	<div class='main-footer__container container'>

		<div class='main-footer__copyright wait-for-animation wait-for-animation-left'>
			<?php
			printf(
				// translators: %1$s: HTML Copyright sign.
				// translators: %2$s: Current date.
				esc_html__( 'Copyright %1$s %2$s maciejruminski.pl', 'TRANSLATE' ),
				'&copy;',
				date( 'Y' ) === '2019' ? date( 'Y' ) : '2019-' . date( 'Y' )
			);
			?>
		</div>

		<address class='main-footer__contact wait-for-animation wait-for-animation-right'>
			<a class='main-footer__contactTel' href='tel:726-939-908'>
				<?php
				printf(
					// translators: %1$s: Start of the HTML span tag.
					// translators: %2$s: End of the HTML span tag.
					esc_html__( '%1$sTel:%2$s 726-939-908', 'TRANSLATE' ),
					'<span class="main-footer__contactTel--bold">',
					'</span>'
				);
				?>
			</a>
			<a class='main-footer__contactEmail' href='mailto:mac-rum@wp.pl'>
				<?php
				printf(
					// translators: %1$s: Start of the HTML span tag.
					// translators: %2$s: End of the HTML span tag.
					esc_html__( '%1$sEmail:%2$s mac-rum@wp.pl', 'TRANSLATE' ),
					'<span class="main-footer__contactEmail--bold">',
					'</span>'
				);
				?>
			</a>
		</address>

	</div>

</footer>

<?php wp_footer(); ?>
