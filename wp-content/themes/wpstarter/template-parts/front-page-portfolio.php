<?php
/**
* Front page portfolio.
*/

$class = 'front-page-portfolio';
?>

<section id='portfolio_section' class='<?php echo esc_attr( "$class to-navigate" ); ?>'>

	<div class='<?php echo esc_attr( "{$class}__container container" ); ?>'>

		<header class='<?php echo esc_attr( "{$class}__header wait-for-animation" ); ?>'>
			<p class='<?php echo esc_attr( "{$class}__subheading wait-for-animation wait-for-animation-top" ); ?>'><?php esc_html_e( 'Kilka moich projektów.', 'TRANSLATE' ); ?></p>
			<h2 class='<?php echo esc_attr( "{$class}__heading wait-for-animation wait-for-animation-right" ); ?>'>
				<?php
				printf(
					// translators: %1$s: Start of the HTML span tag.
					// translators: %2$s: End of the HTML span tag.
					esc_html__( 'Portfolio%1$s.%2$s', 'TRANSLATE' ),
					"<span class='{$class}__headingDot wait-for-animation'>",
					'</span>'
				);
				?>
			</h2>
		</header>

		<ul class='<?php echo esc_attr( "{$class}__list" ); ?>'>

			<?php
			$query = new WP_Query( [
				'post_type'      => 'portfolio',
				'posts_per_page' => -1
			] );

			if ( $query->have_posts() ) :

				$i = 0;

				while ( $query->have_posts() ) : $query->the_post();

					$id          = get_the_id();
					$even_or_odd = $i % 2 === 0 ? 'odd' : 'even'; ?>

					<li class='<?php echo esc_attr( "{$class}__item {$even_or_odd}" ); ?>'>

						<?php $img = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'full' ); ?>

						<picture class='<?php echo esc_attr( "{$class}__imgContainer {$even_or_odd}" ); ?>'>
							<!-- WEBP -->
							<source
								data-srcset='<?php echo esc_url( "{$img[0]}.webp" ); ?>'
								srcset='data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
								type='image/webp'
							>
							<!-- JPEG -->
							<source
								data-srcset='<?php echo esc_url( $img[0] ); ?>'
								srcset='data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
								type='image/jpeg'
							>
							<img
								class='<?php echo esc_attr( "{$class}__img lazy" ); ?>'
								data-src='<?php echo esc_url( $img[0] ); ?>'
								src='data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
								data-aspect-ratio='<?php echo esc_attr( $img[1] / $img[2] ); ?>'
								alt='<?php echo esc_attr( get_post_meta( get_post_thumbnail_id( $id ), '_wp_attachment_image_alt', true ) ); ?>'
							>
						</picture>

						<div class='<?php echo esc_attr( "{$class}__contentContainer" ); ?>'>

							<?php
							$short_description = get_field( 'short_description', $id );

							if ( $short_description ) : ?>
								<p class='<?php echo esc_attr( "{$class}__shortDescription wait-for-animation wait-for-animation-right" ); ?>'><?php echo esc_html( $short_description ); ?></p>
							<?php endif; ?>

							<h3 class='<?php echo esc_attr( "{$class}__title wait-for-animation wait-for-animation-right" ); ?>'>
								<?php
								printf(
									// translators: %1$s: Start of the HTML span tag.
									// translators: %2$s: End of the HTML span tag.
									// translators: %3$s: The title of the project.
									esc_html__( '%1$sNazwa projektu:%2$s %3$s', 'TRANSLATE' ),
									'<span class="screen-reader-text">',
									'</span>',
									esc_html( get_the_title() )
								);
								?>
							</h3>
							<p class='<?php echo esc_attr( "{$class}__excerpt wait-for-animation wait-for-animation-bottom" ); ?>'><?php echo esc_html( get_the_excerpt() ); ?></p>
							<button type='button' aria-controls='<?php echo esc_attr( "descriptions-{$id}" ); ?>' aria-expanded='false' class='<?php echo esc_attr( "{$class}__more wait-for-animation wait-for-animation-bottom" ); ?>'>
								<?php
								printf(
									// translators: %1$s: Start of the HTML span tag.
									// translators: %2$s: End of the HTML span tag.
									// translators: %3$s: The title of the project.
									esc_html__( 'Czytaj więcej %1$sna temat projektu %3$s%2$s', 'TRANSLATE' ),
									'<span class="screen-reader-text">',
									'</span>',
									esc_html( get_the_title() )
								);
								?>
							</button>

							<?php
							$content = wpautop( wp_kses_post( get_the_content() ), false );
							$content = str_replace( '<p>', "<p class='{$class}__description'>", $content ); ?>
							<div id='<?php echo esc_attr( "descriptions-{$id}" ); ?>' aria-hidden='true' class='<?php echo esc_attr( "{$class}__descriptions {$class}__descriptions--height" ); ?>'><?php echo $content; ?></div>

							<?php
							$technologies = get_field( 'technologies', $id );
							$technologies = explode( '|', $technologies );

							if ( is_array( $technologies ) && ! empty( $technologies ) ) : ?>

								<dl class='<?php echo esc_attr( "{$class}__technologies wait-for-animation" ); ?>'>
									<dt class='<?php echo esc_attr( "{$class}__technologiesTitle wait-for-animation wait-for-animation-right" ); ?>'><?php esc_html_e( 'Technologie/narzędzia:', 'TRANSLATE' ); ?></dt>
									<?php foreach ( $technologies as $technology ): ?>
										<dd class='<?php echo esc_attr( "{$class}__technologiesItem wait-for-animation wait-for-animation-top" ); ?>'><?php echo esc_html( $technology ); ?></dd>
									<?php endforeach; ?>
								</dl>

							<?php endif; ?>

							<?php
							$demo       = get_field( 'demo', $id );
							$repository = get_field( 'repository', $id );
							
							if ( $demo || $repository ) : ?>
		
								<dl class='<?php echo esc_attr( "{$class}__links" ); ?>'>
									<dt class='<?php echo esc_attr( "{$class}__linksTitle wait-for-animation wait-for-animation-right" ); ?>'><?php esc_html_e( 'Linki:', 'TRANSLATE' ); ?></dt>
									<?php if ( $demo ) : ?>
										<dd>
											<a
												class='<?php echo esc_attr( "{$class}__linksItem {$class}__linksItem--demo button wait-for-animation wait-for-animation-bottom" ); ?>'
												href='<?php echo esc_url( $demo ); ?>'
												target='_blank'
												rel='noopener noreferrer'
												title='<?php
													echo esc_attr( sprintf(
														// translators: %s: The title of the project.
														esc_html__( 'Zobacz projekt %s na żywo.', 'TRANSLATE' ),
														esc_html( get_the_title() )
													) );
													?>'
											><?php esc_html_e( 'Demo', 'TRANSLATE' ); ?></a>
										</dd>
									<?php endif; ?>
									<?php if ( $repository ) : ?>
										<dd>
											<a
												class='<?php echo esc_attr( "{$class}__linksItem {$class}__linksItem--repository sd_button wait-for-animation wait-for-animation-bottom" ); ?>'
												href='<?php echo esc_url( $repository ); ?>'
												target='_blank'
												rel='noopener noreferrer'
												title='<?php
													echo esc_attr( sprintf(
														// translators: %s: The title of the project.
														esc_html__( 'Zobacz repozytorium projektu %s.', 'TRANSLATE' ),
														esc_html( get_the_title() )
													) );
													?>'
											><?php esc_html_e( 'Repozytorium', 'TRANSLATE' ); ?></a>
										</dd>
									<?php endif; ?>
								</dl>

							<?php endif; ?>

						</div>

					</li>

				<?php $i++; endwhile; ?>

				<?php wp_reset_query(); ?>

			<?php endif; ?>

		</ul>

	</div>

</section>

