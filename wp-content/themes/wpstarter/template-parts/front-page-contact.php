<?php
/**
* Front page contact.
*/

$class = 'front-page-contact';
?>

<section id='contact_section' class='<?php echo esc_attr( "$class to-navigate" ); ?>'>

	<div class='<?php echo esc_attr( "{$class}__container container" ); ?>'>

		<header class='<?php echo esc_attr( "{$class}__header wait-for-animation" ); ?>'>
			<p class='<?php echo esc_attr( "{$class}__subheading wait-for-animation wait-for-animation-top" ); ?>'><?php esc_html_e( 'Masz pytanie? Śmiało, napisz', 'TRANSLATE' ); ?></p>
			<h2 class='<?php echo esc_attr( "{$class}__heading wait-for-animation wait-for-animation-right" ); ?>'>
				<?php
				printf(
					// translators: %1$s: Start of the HTML span tag.
					// translators: %2$s: End of the HTML span tag.
					esc_html__( 'Kontakt%1$s.%2$s', 'TRANSLATE' ),
					"<span class='{$class}__headingDot wait-for-animation'>",
					'</span>'
				);
				?>
			</h2>
		</header>

		<div class='<?php echo esc_attr( "{$class}__form" ); ?>'><?php echo do_shortcode( '[contact-form-7 id="45" title="Contact"]', false ); ?></div>

	</div>

</section>
