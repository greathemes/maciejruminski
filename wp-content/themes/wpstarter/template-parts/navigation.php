<?php
/**
* Primary navigation.
*/

if ( has_nav_menu( 'primary' ) ) : ?>

	<div class='desktop-nav'>

		<?php $logo = get_bloginfo( 'name' ); ?>
		<?php $tagline = get_bloginfo( 'description' ); ?>

		<?php if ( $logo && $tagline ) : ?>
			
			<div class='desktop-nav__identity'>
				<?php if ( $logo ) : ?>
					<p class='desktop-nav__logo wait-for-animation wait-for-animation-right'><?php echo esc_html( get_bloginfo( 'name' ) ); ?></p>
				<?php endif; ?>
				<?php if ( $tagline ) : ?>
					<p class='desktop-nav__tagline wait-for-animation wait-for-animation-right'><?php echo esc_html( get_bloginfo( 'description' ) ); ?></p>
				<?php endif ?>
			</div>

		<?php endif; ?>

		<button type='button' class='desktop-nav__button'>
			<span class='desktop-nav__buttonIcon desktop-nav__buttonIcon--top wait-for-animation' aria-hidden='true'></span>
			<span class='desktop-nav__buttonIcon desktop-nav__buttonIcon--middle wait-for-animation' aria-hidden='true'></span>
			<span class='desktop-nav__buttonIcon desktop-nav__buttonIcon--bottom wait-for-animation' aria-hidden='true'></span>
			<span class='screen-reader-text'><?php esc_html_e( 'Pokaż nawigację', 'TRANSLATE' ); ?></span>
		</button>

		<nav class='desktop-nav__nav'>
			<?php wp_nav_menu( [
				'theme_location' => 'primary',
				'container'      => false,
				'items_wrap'     => '<ul class="desktop-nav__list unstyled">' . '%3$s' . '</ul>',
				'walker'         => new Wpstarter_Walker_Primary( 'desktop-nav', true ),
			] ); ?>
		</nav>
	</div>

	<div class='mobile-nav'>
		<div class='mobile-nav__container'>

			<button type='button' class='mobile-nav__button is-hidden'>
				<span class='mobile-nav__buttonIcon mobile-nav__buttonIcon--top' aria-hidden='true'></span>
				<span class='mobile-nav__buttonIcon mobile-nav__buttonIcon--middle' aria-hidden='true'></span>
				<span class='mobile-nav__buttonIcon mobile-nav__buttonIcon--bottom' aria-hidden='true'></span>
				<span class='screen-reader-text'><?php esc_html_e( 'Schowaj nawigację', 'TRANSLATE' ); ?></span>
			</button>

			<nav class='mobile-nav__nav'>
				<?php wp_nav_menu( [
					'theme_location' => 'primary',
					'container'      => false,
					'items_wrap'     => '<ul class="mobile-nav__list">' . '%3$s' . '</ul>',
					'walker'         => new Wpstarter_Walker_Primary( 'mobile-nav', true ),
				] ); ?>
			</nav>

		</div>
	</div>

<?php endif;
