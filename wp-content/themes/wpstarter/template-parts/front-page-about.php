<?php
/**
* Front page about.
*/

$class = 'front-page-about';
?>

<section id='about_section' class='<?php echo esc_attr( "$class to-navigate" ); ?>'>

	<div class='<?php echo esc_attr( "{$class}__container container" ); ?>'>

		<div class='<?php echo esc_attr( "{$class}__innerContainer" ); ?>'>

			<header class='<?php echo esc_attr( "{$class}__header wait-for-animation" ); ?>'>
				<p class='<?php echo esc_attr( "{$class}__subheading wait-for-animation wait-for-animation-top" ); ?>'><?php esc_html_e( 'Kim jestem?', 'TRANSLATE' ); ?></p>
				<h2 class='<?php echo esc_attr( "{$class}__heading wait-for-animation wait-for-animation-right" ); ?>'>
					<?php
					printf(
						// translators: %1$s: Start of the HTML span tag.
						// translators: %2$s: End of the HTML span tag.
						esc_html__( 'Trochę o  mnie%1$s.%2$s', 'TRANSLATE' ),
						"<span class='{$class}__headingDot wait-for-animation'>",
						'</span>'
					);
					?>
				</h2>
			</header>

			<div class='<?php echo esc_attr( "{$class}__contentContainer" ); ?>'>

				<div class='<?php echo esc_attr( "{$class}__contentInnerContainer" ); ?>'>
					<h3 class='<?php echo esc_attr( "{$class}__title wait-for-animation wait-for-animation-right" ); ?>'><?php esc_html_e( 'Co potrafię?', 'TRANSLATE' ); ?></h3>
					<p class='<?php echo esc_attr( "{$class}__content wait-for-animation wait-for-animation-bottom" ); ?>'><?php esc_html_e( 'Moje umiejętności są dość typowe dla osoby na poziomie Juniora, który głównie tworzy korzystając z WordPressa.', 'TRANSLATE' ); ?></p>
					<p class='<?php echo esc_attr( "{$class}__content wait-for-animation wait-for-animation-bottom" ); ?>'><?php esc_html_e( 'W swoich projektach korzystam z podstawowego PHP (na poziomie WP), HTML, CSS/SCSS, GULP, JavaScript/jQuery. Korzystam również z GIT, ale są to jedynie podstawowe komendy wpisywane z poziomu terminala (diff, status, add, push, itd.).', 'TRANSLATE' ); ?></p>
					<p class='<?php echo esc_attr( "{$class}__content wait-for-animation wait-for-animation-bottom" ); ?>'><?php esc_html_e( 'Odnośnie WordPressa, rozumiem działanie filtrów i akcji, wiem jak korzystać z WordPress Codex, rozumiem co to jest "plugin territory". Pracowałem też z Custom Post Types oraz Taxonomy. Odnośnie pluginów korzystałem np. z Kirki, ACF czy WPML. Mam niestety obecnie bardzo niewielkie doświadczenie z WooCommerce.', 'TRANSLATE' ); ?></p>
					<p class='<?php echo esc_attr( "{$class}__content wait-for-animation wait-for-animation-bottom" ); ?>'><?php esc_html_e( 'Jeśli chodzi o kwestie związane z projektowaniem to myślę, że mam jako takie poczucie estetyki ale z całą pewnością nie jestem grafikiem i zdecydowanie wolę dostać gotowy projekt. Znam podstawy Photoshop czy Affinity Designer.', 'TRANSLATE' ); ?></p>
					<p class='<?php echo esc_attr( "{$class}__content wait-for-animation wait-for-animation-bottom" ); ?>'><?php esc_html_e( 'Staram się również zwiększać swoją wiedzę nt. optymalizowania strony (kompresja zdjęć, lazy loading, itd.) na podstawie raportu ze strony GTmetrix czy PageSpeed Insights. Nie znoszę wolnych stron i staram się aby moje witryny działały możliwie szybko. Obecnie sporo czytam również o accessibility, treść powinna być dostępna dla każdego.', 'TRANSLATE' ); ?></p>
					<p class='<?php echo esc_attr( "{$class}__content wait-for-animation wait-for-animation-bottom" ); ?>'><?php esc_html_e( 'Ostatnia kwestia to angielski, który u mnie sprowadza się do czytania. Pisanie, mówienie i słuchanie wypada niestety gorzej o czym przekonałem się w Anglii (co prawda pracuję nad tym, ale nie jest to niestety umiejętność, którą da się wyraźnie poprawić w krótkim czasie).', 'TRANSLATE' ); ?></p>

				</div>

				<div class='<?php echo esc_attr( "{$class}__contentInnerContainer" ); ?>'>
					<h3 class='<?php echo esc_attr( "{$class}__title wait-for-animation wait-for-animation-right" ); ?>'><?php esc_html_e( 'Dlaczego programowanie?', 'TRANSLATE' ); ?></h3>
					<p class='<?php echo esc_attr( "{$class}__content wait-for-animation wait-for-animation-bottom" ); ?>'><?php esc_html_e( 'Nie będę tutaj pisał o pasji, której początki miały miejsce przed narodzinami, ponieważ po pierwsze, pisanie tego wydaje mi się już bardzo oklepane, a po drugie, nic takiego nie miało miejsca, moje wejście w świat kodu to zwykły (ale przyjemny) przypadek :)', 'TRANSLATE' ); ?></p>
					<p class='<?php echo esc_attr( "{$class}__content wait-for-animation wait-for-animation-bottom" ); ?>'><?php esc_html_e( 'Pewnego dnia wrzuciłem na forum graficzne projekt strony internetowej i jeden z użytkowników napisał, że zrobienie do tego sensownego HTMLa i CSSa zajmie wieki (i miał rację, nie znałem wtedy jeszcze żadnych zasad tworzenia stron, projekt był po prostu kiepski).', 'TRANSLATE' ); ?></p>
					<p class='<?php echo esc_attr( "{$class}__content wait-for-animation wait-for-animation-bottom" ); ?>'><?php esc_html_e( 'Jako, że nie miałem pojęcia czym jest HTML, wpisałem ową frazę w Google, wklepałem pierwsze "Hello World" i zwyczajnie zaciekawiłem się tematem ruszając dalej i pozostając już na tej ścieżce. Zacząłem od absolutnego zera, nie uczestniczyłem w Bootcampach, płatnych szoleniach i tego typu rzeczach, jestem typowym samoukiem, wymyślam coś i z pomocą internetu zaczynam "grzebać".', 'TRANSLATE' ); ?></p>
					<p class='<?php echo esc_attr( "{$class}__content wait-for-animation wait-for-animation-bottom" ); ?>'><?php esc_html_e( 'Dlaczego to robię? Tworzenie schludnych, szybkich, fajnych stron daje mi po prostu frajdę i satysfakcję.', 'TRANSLATE' ); ?></p>

					<h3 class='<?php echo esc_attr( "{$class}__title wait-for-animation wait-for-animation-right" ); ?>'><?php esc_html_e( 'Króciutko o mnie.', 'TRANSLATE' ); ?></h3>
					<p class='<?php echo esc_attr( "{$class}__content wait-for-animation wait-for-animation-bottom" ); ?>'><?php esc_html_e( 'Raczej łatwo nawiązuję kontakty, jestem chętny do pracy i zmotywowany. Potrafię utrzymać koncentrację na zadaniu, nie myśląc w tym czasie o Facebookach (co nie znaczy, że jestem ponurakiem). Wg. byłych kolegów z pracy, siedzenie ze mną w biurze to prędzej przyjemność niż katusze :)', 'TRANSLATE' ); ?></p>
				</div>
				

			</div>

			<div class='<?php echo esc_attr( "{$class}__cv wait-for-animation wait-for-animation-left" ); ?>'>
				<a class='<?php echo esc_attr( "{$class}__cvLink button" ); ?>' href='<?php echo esc_url( get_permalink( get_theme_mod( 'wpstarter_cv_page' ) ) ); ?>' title='<?php esc_attr_e( 'Przejdź do strony z moim Curriculum Vitae.', 'TRANSLATE' ) ?>'><?php esc_html_e( 'Zobacz moje CV', 'TRANSLATE' ) ?></a>
				<a class='<?php echo esc_attr( "{$class}__cvLink sd_button" ); ?>' href='<?php echo esc_url( wp_get_attachment_url( get_theme_mod( 'wpstarter_cv' ) ) ); ?>' target='_blank' title='<?php esc_attr_e( 'Zobacz moje Curriculum Vitae w formacie PDF.', 'TRANSLATE' ) ?>'><?php esc_html_e( 'CV w PDF', 'TRANSLATE' ) ?></a>
			</div>

		</div>

	</div>

</section>
