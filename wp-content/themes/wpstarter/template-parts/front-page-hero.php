<?php
/**
* Front page hero.
*/

$class = 'front-page-hero';
$img   = wp_get_attachment_image_src( get_theme_mod( 'wpstarter_hero_img' ), 'full' );
?>

<section id='hero_section' class='<?php echo esc_attr( "$class to-navigate" ); ?>'>

	<picture aria-hidden='true'>
		<!-- WEBP -->
		<source
			data-srcset='<?php echo esc_url( "{$img[0]}.webp" ); ?>'
			srcset='data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
			type='image/webp'
		>
		<!-- JPEG -->
		<source
			data-srcset='<?php echo esc_url( $img[0] ); ?>'
			srcset='data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
			type='image/npg'
		>
		<img
			class='<?php echo esc_attr( "{$class}__img lazy" ); ?>'
			data-src='<?php echo esc_url( $img[0] ); ?>'
			src='data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
			data-aspect-ratio='<?php echo esc_attr( $img[1] / $img[2] ); ?>'
			alt='<?php echo esc_attr( get_post_meta( get_post_thumbnail_id( $id ), '_wp_attachment_image_alt', true ) ); ?>'
		>
	</picture>

	<div class='<?php echo esc_attr( "{$class}__container container" ); ?>'>

		<div class='<?php echo esc_attr( "{$class}__innerContainer" ); ?>'>

			<header class='<?php echo esc_attr( "{$class}__header" ); ?>'>
				<p class='<?php echo esc_attr( "{$class}__occupation wait-for-animation wait-for-animation-right" ); ?>'>
					<?php
					printf(
						// translators: %1$s: Start of the HTML span tag.
						// translators: %2$s: End of the HTML span tag.
						esc_html__( '%1$sMoja profesja to%2$s Junior WordPress Developer', 'TRANSLATE' ),
						'<span class="screen-reader-text">',
						'</span>'
					);
					?>		
				</p>
				<h1 class='<?php echo esc_attr( "{$class}__heading wait-for-animation wait-for-animation-right" ); ?>'>	
					<?php
					printf(
						// translators: %1$s: Start of the HTML span tag.
						// translators: %2$s: End of the HTML span tag.
						esc_html__( 'Hej, nazywam się Maciej i witam Cię na moim portfolio%1$s!%2$s', 'TRANSLATE' ),
						"<span class='{$class}__headingExclamation wait-for-animation wait-for-animation-top'>",
						'</span>'
					);
					?>
				</h1>
				<p class='<?php echo esc_attr( "{$class}__subheading wait-for-animation wait-for-animation-right" ); ?>'>
					<?php
					printf(
						// translators: %1$s: Start of the HTML span tag.
						// translators: %2$s: End of the HTML span tag.
						esc_html__( 'Tworzę strony internetowe w oparciu o CMS WordPress i %1$sobecnie szukam stałej pracy/stażu na terenie Krakowa%2$s, aby zwiększyć swoje umiejętności pracując u boku bardziej doświadczonych kolegów/koleżanek.', 'TRANSLATE' ),
						"<span class='{$class}__subheadingUnderline'>",
						'</span>'
					);
					?>
				</p>
			</header>

			<a href='#portfolio_section' class='<?php echo esc_attr( "{$class}__link button wait-for-animation wait-for-animation-top" ); ?>' title='<?php esc_attr_e( 'Przejdź do sekcji portfolio', 'TRANSLATE' ) ?>'><?php esc_html_e( 'Zobacz projekty', 'TRANSLATE' ); ?></a>
			<a href='#about_section' class='<?php echo esc_attr( "{$class}__link sd_button wait-for-animation wait-for-animation-bottom" ); ?>' title='<?php esc_attr_e( 'Przejdź do sekcji o mnie', 'TRANSLATE' ) ?>'><?php esc_html_e( 'Poznaj mnie bliżej', 'TRANSLATE' ); ?></a>

		</div>

	</div>

</section>
