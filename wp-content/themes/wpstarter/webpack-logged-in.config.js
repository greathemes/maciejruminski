import yargs from 'yargs';
import webpack from 'webpack';

module.exports = {
	mode: yargs.argv.prod ? 'production' : 'development',
	entry: [
		'@babel/polyfill',
		'./scripts/main-logged-in.js'
	],
	output: {
		filename: 'scripts-logged-in.min.js'
	},
	externals: {
		jquery: 'jQuery'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules\/(?!bullets-js)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: [ '@babel/preset-env' ]
					}
				}
			}
		]
	}
}
