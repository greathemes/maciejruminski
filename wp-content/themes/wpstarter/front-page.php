<?php
/**
* Front page.
*/

get_header();

get_template_part( 'template-parts/front-page', 'hero' );
get_template_part( 'template-parts/front-page', 'portfolio' );
get_template_part( 'template-parts/front-page', 'about' );
get_template_part( 'template-parts/front-page', 'contact' );

get_footer();
